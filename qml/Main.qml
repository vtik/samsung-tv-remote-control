/*
 * Copyright (C) 2022  vtik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * samsungtvremote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "."

import Example 1.0

MainView {
    id: main
    objectName: 'mainView'
    applicationName: 'samsungtvremote.vtik'
    automaticOrientation: false

    PageStack {
        id: pageStack
        Component.onCompleted:
        {
            push(pageRemote)
        }
        Page {
            id: pageRemote
            visible: false

            Timer {
                interval: 5000; running: true; repeat: true
                onTriggered:
                {
                    if(pageRemote.visible)
                        pageRemote.sendKeyAndUpdate("")
                }
            }

            header: PageHeader {
                id: header
                property alias text: noteActionIcon.text
                contents: Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    spacing: units.gu(1)

                    Text {
                        anchors.fill: parent
                        id: noteActionIcon
                        text: "TV not connected !"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
                trailingActionBar.numberOfSlots: 1
                trailingActionBar.actions: [
                    Action {
                        text: i18n.tr("Settings")
                        iconName: "settings"

                        onTriggered: {
                            pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
                        }
                    }
                ]
            }


            function sendKeyAndUpdate(key) {
                if(pageRemote.visible){
                    if (Example.isConnected()){
                        pageRemote.header.text = "TV connected !"
                        Example.sendKey(key)
                    }
                    else
                        pageRemote.header.text = "TV not connected !"
                }
            }

            property int hunit : height / 71
            property int wunit : width / 45

                Rectangle{
                    id: row1
                    width: parent.width
                    height: pageRemote.hunit * 8
                    y: header.height
                    property int hspace : pageRemote.wunit * 6
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:power
                        height: parent.height - row1.vspace
                        width: parent.width / 3  - row1.hspace
                        text: "Power"
                        y: row1.vspace/2
                        x:row1.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_POWER")
                    }
                    Button{
                        id:mute
                        width: parent.width / 3 - row1.hspace
                        height: parent.height  - row1.vspace
                        text: "mute"
                        x: parent.width * 1/3 + row1.hspace/2
                        y: row1.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_MUTE")
                    }
                    Button{
                        id:source
                        width: parent.width / 3 - row1.hspace
                        height: parent.height - row1.vspace
                        text: "Source"
                        x: parent.width * 2/3 + row1.hspace/2
                        y: row1.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_SOURCE")
                    }
                }

                Rectangle{
                    id: row2
                    width: parent.width
                    height: pageRemote.hunit * 19
                    y: header.height + pageRemote.hunit * 10
                    property int hspace : pageRemote.wunit * 2
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:chup
                        height: parent.height/2
                        width: (parent.width/5) - row2.hspace
                        text: "Ch+"
                        x: 3 * row2.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_CHUP")
                    }
                    Button{
                        id:chdown
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/2
                        text: "Ch-"
                        y: parent.height/2
                        x: 3 * row2.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_CHDOWN")
                    }
                    Button{
                        id:volup
                        height: parent.height/2
                        width: (parent.width/5) - row2.hspace
                        text: "Vol+"
                        x:parent.width - width - (3 * row2.hspace/2)
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_VOLUP")
                    }
                    Button{
                        id:voldown
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/2
                        text: "Vol-"
                        y: parent.height/2
                        x: parent.width - width - (3 * row2.hspace/2)
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_VOLDOWN")
                    }

                    Button{
                        id:up
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/3
                        text: "Up"
                        x: pageRemote.wunit * 18 + row2.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_UP")
                    }

                    Button{
                        id:left
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/3
                        text: "Left"
                        x: pageRemote.wunit * 9 + row2.hspace/2 * 3
                        y: parent.height/3
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_LEFT")
                    }
                    Button{
                        id:ok
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/3
                        text: "OK"
                        x: pageRemote.wunit * 18 + row2.hspace/2
                        y: parent.height/3
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_ENTER")
                    }
                    Button{
                        id:right
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/3
                        text: "Right"
                        x: pageRemote.wunit * 27 - row2.hspace/2
                        y: parent.height/3
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_RIGHT")
                    }

                    Button{
                        id:down
                        width: (parent.width/5) - row2.hspace
                        height: parent.height/3
                        text: "Down"
                        x: pageRemote.wunit * 18 + row2.hspace/2
                        y: parent.height/3 * 2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_DOWN")
                    }
                }

                Rectangle{
                    id: row3
                    width: parent.width
                    height: pageRemote.hunit * 8
                    y: header.height + pageRemote.hunit * 30
                    property int hspace : pageRemote.wunit * 6
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:ret
                        height: parent.height - row1.vspace
                        width: parent.width / 3  - row1.hspace
                        text: "Back"
                        y: row1.vspace/2
                        x:row1.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_RETURN")
                    }
                    Button{
                        id:menu
                        width: parent.width / 3 - row1.hspace
                        height: parent.height  - row1.vspace
                        text: "Menu"
                        x: parent.width * 1/3 + row1.hspace/2
                        y: row1.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_MENU")
                    }
                    Button{
                        id:exit
                        width: parent.width / 3 - row1.hspace
                        height: parent.height - row1.vspace
                        text: "Tools"
                        x: parent.width * 2/3 + row1.hspace/2
                        y: row1.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_TOOLS")
                    }
                }

                Rectangle{
                    id: row4
                    width: parent.width
                    height: pageRemote.hunit * 6
                    y: header.height + pageRemote.hunit * 39
                    property int hspace : pageRemote.wunit * 1
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:f2
                        width: parent.width / 4 - row4.hspace
                        height: parent.height  - row4.vspace
                        text: "Info"
                        x: parent.width * 1/4 + row4.hspace/2
                        y: row4.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_INFO")
                    }
                    Button{
                        id:f3
                        width: parent.width / 4 - row4.hspace
                        height: parent.height - row4.vspace
                        text: "Guide"
                        x: parent.width * 2/4 + row4.hspace/2
                        y: row4.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_GUIDE")
                    }
                }

                Rectangle{
                    id: row5
                    width: parent.width
                    height: pageRemote.hunit * 6
                    y: header.height + pageRemote.hunit * 45
                    property int hspace : pageRemote.wunit * 1
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:btn1
                        height: parent.height - row5.vspace
                        width: parent.width / 4  - row5.hspace
                        text: "1"
                        y: row5.vspace/2
                        x:row5.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_1")
                    }
                    Button{
                        id:btn2
                        width: parent.width / 4 - row5.hspace
                        height: parent.height  - row5.vspace
                        text: "2"
                        x: parent.width * 1/4 + row5.hspace/2
                        y: row5.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_2")
                    }
                    Button{
                        id:btn3
                        width: parent.width / 4 - row5.hspace
                        height: parent.height - row5.vspace
                        text: "3"
                        x: parent.width * 2/4 + row5.hspace/2
                        y: row5.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_3")
                    }
                    Button{
                        id:btn4
                        width: parent.width / 4 - row5.hspace
                        height: parent.height - row5.vspace
                        text: "4"
                        x: parent.width * 3/4 + row5.hspace/2
                        y: row5.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_4")
                    }
                }

                Rectangle{
                    id: row6
                    width: parent.width
                    height: pageRemote.hunit * 6
                    y: header.height + pageRemote.hunit * 51
                    property int hspace : pageRemote.wunit * 1
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:btn5
                        height: parent.height - row6.vspace
                        width: parent.width / 4  - row6.hspace
                        text: "5"
                        y: row6.vspace/2
                        x:row6.hspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_5")
                    }
                    Button{
                        id:btn6
                        width: parent.width / 4 - row6.hspace
                        height: parent.height  - row6.vspace
                        text: "6"
                        x: parent.width * 1/4 + row6.hspace/2
                        y: row6.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_6")
                    }
                    Button{
                        id:btn7
                        width: parent.width / 4 - row6.hspace
                        height: parent.height - row6.vspace
                        text: "7"
                        x: parent.width * 2/4 + row6.hspace/2
                        y: row6.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_7")
                    }
                    Button{
                        id:btn8
                        width: parent.width / 4 - row6.hspace
                        height: parent.height - row6.vspace
                        text: "8"
                        x: parent.width * 3/4 + row6.hspace/2
                        y: row6.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_8")
                    }
                }

                Rectangle{
                    id: row7
                    width: parent.width
                    height: pageRemote.hunit * 6
                    y: header.height + pageRemote.hunit * 57
                    property int hspace : pageRemote.wunit * 1
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:btn9
                        width: parent.width / 4 - row7.hspace
                        height: parent.height  - row7.vspace
                        text: "9"
                        x: parent.width * 1/4 + row7.hspace/2
                        y: row7.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_9")
                    }
                    Button{
                        id:btn0
                        width: parent.width / 4 - row7.hspace
                        height: parent.height - row7.vspace
                        text: "0"
                        x: parent.width * 2/4 + row7.hspace/2
                        y: row7.vspace/2
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_0")
                    }
                }

                Rectangle{
                    id: row8
                    width: parent.width
                    height: pageRemote.hunit * 6
                    y: header.height + pageRemote.hunit * 63
                    property int hspace : pageRemote.wunit * 1
                    property int vspace : pageRemote.hunit * 2
                    Button{
                        id:btnRed
                        height: parent.height - row8.vspace
                        width: parent.width / 4  - row8.hspace
                        y: row8.vspace/2
                        x:row8.hspace/2
                        color: "red"
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_RED")
                    }
                    Button{
                        id:btnGreen
                        width: parent.width / 4 - row8.hspace
                        height: parent.height  - row8.vspace
                        x: parent.width * 1/4 + row8.hspace/2
                        y: row8.vspace/2
                        color: "green"
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_GREEN")
                    }
                    Button{
                        id:btnYellow
                        width: parent.width / 4 - row8.hspace
                        height: parent.height - row8.vspace
                        x: parent.width * 2/4 + row8.hspace/2
                        y: row8.vspace/2
                        color: "yellow"
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_YELLOW")
                    }
                    Button{
                        id:btnBlue
                        width: parent.width / 4 - row8.hspace
                        height: parent.height - row8.vspace
                        x: parent.width * 3/4 + row8.hspace/2
                        y: row8.vspace/2
                        color: "blue"
                        onClicked: pageRemote.sendKeyAndUpdate("KEY_BLUE")
                    }
                }
        }
    }
}
