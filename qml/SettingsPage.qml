/*
 * Copyright: 2019 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import Example 1.0

Page {
    id: root
    objectName: "settingsPage"
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr("Settings")
    }

    Column {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: units.gu(2)
        spacing: 2

        Row {
            spacing: units.gu(2)
            width: parent.width

            Label{
                text : "TV IP address :"
                height: units.gu(5)
                width: parent.width/3
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: ipAddress
                placeholderText: Example.getIp()
                width: parent.width * 2/3 - units.gu(1)
                height: units.gu(5)
                x:parent.width/3
                verticalAlignment: Text.AlignVCenter
            }
        }

        Row {
            width: parent.width
            spacing: units.gu(2)

            Label{
                text : "TV Port :"
                height: units.gu(5)
                width: parent.width/3
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: port
                placeholderText: Example.getPort()
                width: parent.width * 2/3 - units.gu(1)
                height: units.gu(5)
                x:parent.width/3
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    onVisibleChanged: {
        if(!visible){
            if(ipAddress.text != "")
                Example.setIpAdress(ipAddress.text)
            if(port.text != "")
                Example.setPort(port.text)
        }
    }
}
