/*
 * Copyright (C) 2022  vtik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * samsungremotecpp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REMOTE_CONTROL_H
#define REMOTE_CONTROL_H

#include <QObject>
#include <QtWebSockets/QWebSocket>

class Example: public QObject
{
    Q_OBJECT
public:
    explicit Example(bool debug = false, QObject *parent = nullptr);
    ~Example();

    bool isvalid();
    bool loadSettings();
    void writeSettings();
    void setup();

    Q_INVOKABLE void sendKey(QString value);
    Q_INVOKABLE void setIpAdress(QString value);
    Q_INVOKABLE void setPort(QString port);
    Q_INVOKABLE QString getIp();
    Q_INVOKABLE QString getPort();
    Q_INVOKABLE bool isConnected();

Q_SIGNALS:
    void closed();

private Q_SLOTS:
    void onConnected();
    void onMessageReceived(QString message);
    void onError(QAbstractSocket::SocketError error);

private:
    QWebSocket m_webSocket;
    bool m_connected;

    bool m_debug;

    QString m_sSettingsFile;
    bool m_foundSettings;
    QString m_iptv, m_port, m_token;
};

#endif
