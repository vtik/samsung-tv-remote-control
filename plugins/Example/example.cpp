/*
 * Copyright (C) 2022  vtik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * samsungremotecpp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QCoreApplication>
#include <QSettings>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStandardPaths>

#include "example.h"

Example::Example(bool debug, QObject *parent) :
    QObject(parent),
    m_connected(false),
    m_debug(debug),
    m_port("8002")
{
    m_sSettingsFile =  QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/settings.ini";
    m_foundSettings = loadSettings();

    setup();
}

Example::~Example()
{
    m_webSocket.close();
}

QString Example::getIp()
{
    return m_iptv;
}

QString Example::getPort()
{
    return m_port;
}

bool Example::isConnected()
{
    return m_connected;
}

void Example::setup()
{
    if(m_webSocket.isValid())
        m_webSocket.abort();

    m_connected = false;

    QString opening_url = "wss://";
    opening_url += m_iptv + ":" + m_port;
    opening_url += "/api/v2/channels/samsung.remote.control?name=ExampleTVtik06&token=";
    opening_url += m_token;

    if (m_debug)
    {
        qDebug() << "found settings file :" << m_foundSettings;
        qDebug() << "WebSocket server:" << opening_url;
    }

    connect(&m_webSocket, &QWebSocket::connected, this, &Example::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &Example::closed);
    connect( &m_webSocket, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error), [](QAbstractSocket::SocketError error) {
        qDebug() << "received error: " << error;
    });

    typedef void (QWebSocket:: *sslErrorsSignal)(const QList<QSslError> &);
    connect( &m_webSocket, static_cast<sslErrorsSignal>(&QWebSocket::sslErrors), this, [this]( const QList<QSslError> &errors )
    {
        foreach (QSslError error, errors) {
            qDebug() << "ignoring error: " << error;
        }
        m_webSocket.ignoreSslErrors( errors );
    });

    QSslConfiguration config = m_webSocket.sslConfiguration();
    config.setPeerVerifyMode(QSslSocket::VerifyNone);
    m_webSocket.setSslConfiguration(config);
    m_webSocket.open(QUrl(opening_url));
}

void Example::sendKey(QString value) {
    if(!m_connected)
        return;
    if (m_debug)
        qDebug() << "IP : " << m_iptv << " - Sending Key : " << value;
    QString payload = "{\"method\": \"ms.remote.control\", \"params\": {\"Cmd\": \"Click\", \"DataOfCmd\": \""
            + value + "\", \"Option\": \"false\", \"TypeOfRemote\": \"SendRemoteKey\"}}";
    m_webSocket.sendTextMessage(payload);
}

void Example::onConnected()
{
    if (m_debug)
        qDebug() << "WebSocket connected";
    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &Example::onMessageReceived);
    writeSettings();
    m_connected = true;
}

void Example::onMessageReceived(QString message)
{
    if (m_debug)
        qDebug() << "Message received:" << message;
    QJsonDocument doc = QJsonDocument::fromJson(message.toUtf8());
    QJsonObject obj = doc.object();
    QString event = obj["event"].toString();
    if(event == "ms.channel.connect"){
        QJsonObject data = obj["data"].toObject();
        QString token = data["token"].toString();
        if (m_debug)
            qDebug() << "Received Token:" << token;
        if (token != ""){
            m_token = token;
            writeSettings();
        }
    }
}

void Example::onError(QAbstractSocket::SocketError error)
{
    if (m_debug)
        qDebug() << "Error received:" << error;
}

// Settings
void Example::setIpAdress(QString value)
{
    if (m_debug)
      qDebug() << "trying to set tv address to " << value;

    QHostAddress address(value);
    if (QAbstractSocket::IPv4Protocol != address.protocol())
    {
        if (m_debug)
            qDebug() << "given IPv4 address " << value << " is not valid.";
        return;
    }

    if (m_debug)
      qDebug() << "tv address set to " << value;

    m_iptv = value;
    setup();
}

void Example::setPort(QString port)
{
    if (m_debug)
      qDebug() << "trying to set tv port to " << port;

    m_port = port;
    setup();
}

bool Example::loadSettings()
{
    if (m_debug)
        qDebug() << "m_sSettingsFile:" << m_sSettingsFile;

    if(!QFile::exists(m_sSettingsFile))
        return false;

    // .ini format
    QSettings settings(m_sSettingsFile, QSettings::IniFormat);
    m_iptv = settings.value("ip", "").toString();
    m_port = settings.value("port", "8002").toString();
    m_token = settings.value("token", "").toString();

    if(m_token == "")
        return false;

    return true;
}

void Example::writeSettings()
{
    if(!m_connected) return;
    if (m_debug)
        qDebug() << "Saving settings to:" << m_sSettingsFile;
    QSettings settings(m_sSettingsFile, QSettings::IniFormat);
    settings.setValue("ip", m_iptv);
    settings.setValue("port", m_port);
    settings.setValue("token", m_token);
    settings.sync();
}
